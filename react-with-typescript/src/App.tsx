import { FC, useEffect, useState } from "react"
import './App.css'
import axios from "axios";
import User from "./components/User.tsx";
import { AppProps, User as UserInterface } from "./App.types.ts";

const App: FC<AppProps> = ({title}) => {
  const [users, setUsers] = useState<UserInterface[]>([])
  const [isLoading, setIsLoading] = useState(false) // there is a default value, so Typescript automatically infers the type of data we will be storing
  const [username, setUsername] = useState('');

  const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setUsername(event.target.value);
  };

  // useEffect(() => {
  //   const getUsers = async () => {
  //     try {
  //       setIsLoading(true)
  //       const {data} = await axios.get('https://randomuser.me/api/?results=10')
  //       console.log(data.results)
  //       setUsers(data.results)
  //     } catch (error) {
  //       console.log(error)
  //     } finally {
  //       setIsLoading(false)
  //     }
  //   }
  //   getUsers()
  // }, []) // empty array to avoid fetching again

  const handleClick = async () => {
    try {
      setIsLoading(true);
      const { data } = await axios.get('https://randomuser.me/api/?results=10');
      console.log(data);
      setUsers(data.results);
    } catch (error) {
      console.log(error);
    } finally {
      setIsLoading(false);
    }
  };

  return (
    <div>
      <h1>{title}</h1>

      <button onClick={handleClick}>Show Users</button>

      {/* <input type='text' onChange={(event) => {}} /> */}{/* to get the type of event from the ide */}
      <input type='text' onChange={handleChange} />
      <div>{username}</div>

      {isLoading && <p>Loading...</p>}
      <ul>
        {users.map(({ login, name, email }) => {
          return <User key={login.uuid} name={name} email={email}></User>
        })}
      </ul>
    </div>
  )
}

export default App
