import React from 'react'
import ReactDOM from 'react-dom/client'
import App from './App.tsx'
import './index.css'

ReactDOM.createRoot(document.getElementById('root')!).render(
  // <React.StrictMode> // would send the API requests twice in dev environment
    <App title="Some users" />
  // </React.StrictMode>,
)
