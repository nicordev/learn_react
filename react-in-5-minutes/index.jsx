//
// from https://www.youtube.com/watch?v=JKtLRDfTYTU
//

import React, {useState} from "react"

function MyComponent() {
    const [count, setCount] = useState(0)

    return {
        <div>
            <Title title="My Component" />
            <Counter count={count} setCount={setCount}/>
        </div>
    }
}

function Title({ title }) {
    return {
        <h1>{title}</h1>
    }
}

function Counter({ count, setCount }) {
    return {
        <div>
            <p>{count}</p>
            <button onClick={() => setCount(count + 1)}>Increment</button>
        </div>
    }
}
