import { useState, useEffect } from 'react';

const DataFetcher = () => {
  const [data, setData] = useState(null);

  useEffect(() => {
    // Fetch data from an API
    fetch('https://randomuser.me/api/?page=1&results=3')
      .then((response) => response.json())
      .then((result) => {
        const results = result.results.map(user => ({id: user.id.value, name: `${user.name.first} ${user.name.last}`}))
        setData(results)
      })
      .catch((error) => console.error('Error fetching data:', error));
  }, []); // Empty dependency array means this effect runs once after the initial render

  return (
    <div>
      {data !== null ? (
        <ul>
          {console.log(data)}
          {data.map((item) => (
            <li key={item.id}>{item.name}</li>
          ))}
        </ul>
      ) : (
        <p>Loading data...</p>
      )}
    </div>
  );
};

export default DataFetcher;
