import React, { useState, useEffect } from 'react';
import { useAuth } from '../Authentication/AuthContext';

const UserProfile = () => {
  const { isAuthenticated } = useAuth();
  const [userData, setUserData] = useState(null);

  useEffect(() => {
    if (isAuthenticated) {
      // Fetch user data when authenticated
      fetch('https://randomuser.me/api/?page=1&results=3')
      .then((response) => response.json())
      .then((result) => {
        const results = result.results.map(user => ({
          id: user.id.value, 
          name: `${user.name.first} ${user.name.last}`,
          email: user.email,
        }))
        setUserData(results)
      })
      .catch((error) => console.error('Error fetching data:', error));
    }
  }, [isAuthenticated]); // Run the effect when isAuthenticated changes

  return (
    <div>
      {isAuthenticated ? (
        <div>
          <h2>Welcome, {userData ? userData.name : 'User'}!</h2>
          <p>Email: {userData ? userData.email : 'Loading...'}</p>
        </div>
      ) : (
        <p>Please log in to view your profile.</p>
      )}
    </div>
  );
};

export default UserProfile;
