import Counter from "./Counter/Counter.jsx";
import DataFetcher from "./DataFetcher/DataFetcher.jsx";
import Timer from "./Timer/Timer.jsx";
import AuthStatus from "./Authentication/AuthStatus.jsx";
import UserProfile from "./UserProfile/UserProfile.jsx";

function App() {
  return (
    <div>
      <Counter></Counter>
      <DataFetcher></DataFetcher>
      <Timer></Timer>
      {/* <AuthStatus></AuthStatus> */}
      <UserProfile></UserProfile>
    </div>
  );
}

export default App;
